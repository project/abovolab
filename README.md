# Abovolab

A pattern-lab dependent base theme that allows quick and painless integration
of most common Drupal components and fast subtheming.

## Requirements

### [drupal/components](https://drupal.org/project/components)
The `components` module is required. Once [this issue](https://www.drupal.org/project/drupal/issues/474684)
is resolved you will also be alerted about this. Until then you must install it
before enabling `abovolab` theme.

### [zehnplus/patternlab](https://bitbucket.org/zehnplus/patternlab)
This theme is **not** a standalone theme. You need a specific patternlab folder structure located under:
`drupal-root/patternlab`.

### [zehnplus/pl_common](https://bitbucket.org/zehnplus/pl_common)
The common components library as it was created by **Abovo Design**. It **must** exist under:
`drupal-root/patternlab/source/_patterns/03-components/00-common`

### [drupal/abovolab](https://drupal.org/project/abovolab)
This theme! Must be under:
`drupal-root/themes/[contrib|custom]/[theme_name]`
So, patternlab **must** be three (3) levels higher than this theme.

If you use drupal-composer/drupal-project, _which you should anyway_, then you are good to go.

## Conception
It was clear to us that some Drupal components do not change much from project to project.
Their CSS does, but their markup is mostly the same. Since our patterlab source files
don't change, we could have their integration ready on all our new Drupal projects in seconds.

## Maintainers & Credits
* Bill Seremetis
  bill@seremetis.net
  drupal.org/u/bserem
* Aris Magripis
  aris@abovodesign.gr
  drupal.org/u/arismag
* George Kaffezas
  george@abovodesign.gr
  drupal.org/u/gkffzs

## Supporting Companies
[Abovo](www.abovodesign.gr)
[zehnplus](www.zehnplus.ch)
[SRM](srm.gr)
